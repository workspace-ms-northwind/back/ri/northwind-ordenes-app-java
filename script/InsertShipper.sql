
INSERT INTO tb_nw_shippers(shipper_id, company_name, phone) VALUES (1, 'Speedy Express', '(503) 555-9831');
INSERT INTO tb_nw_shippers(shipper_id, company_name, phone) VALUES (2, 'United Package', '(503) 555-3199');
INSERT INTO tb_nw_shippers(shipper_id, company_name, phone) VALUES (3, 'Federal Shipping', '(503) 555-9931');
INSERT INTO tb_nw_shippers(shipper_id, company_name, phone) VALUES (4, 'Alliance Shippers', '1-800-222-0451');
INSERT INTO tb_nw_shippers(shipper_id, company_name, phone) VALUES (5, 'UPS', '1-800-782-7892');
INSERT INTO tb_nw_shippers(shipper_id, company_name, phone) VALUES (6, 'DHL', '1-800-225-5345');