package com.northwind.ordenes.controllers;

import com.northwind.ordenes.dto.OrderRequestDTO;
import com.northwind.ordenes.dto.OrderResponseDTO;
import com.northwind.ordenes.dto.PageRequestDTO;
import com.northwind.ordenes.service.IOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/orders")
public class OrderControllers {

    private final IOrderService service;

    @GetMapping
    public ResponseEntity<Page<OrderResponseDTO>> findAll(@RequestBody PageRequestDTO pageRequestDTO){
        return new ResponseEntity<>(service.findAll(pageRequestDTO), HttpStatus.OK) ;
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<OrderResponseDTO> findById(@PathVariable(name = "id") Long id){
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK) ;
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<OrderResponseDTO> save(@Valid @RequestBody OrderRequestDTO entity){
        return ResponseEntity.status(HttpStatus.CREATED).body(service.save(entity));
    }


    @PatchMapping(consumes = "application/json")
    public ResponseEntity<OrderResponseDTO> deleteEntity(@RequestBody OrderRequestDTO entity){
        return new ResponseEntity<>(service.changeStatus(entity), HttpStatus.NO_CONTENT);
    }




}
