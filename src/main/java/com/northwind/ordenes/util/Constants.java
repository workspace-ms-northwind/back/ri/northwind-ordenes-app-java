package com.northwind.ordenes.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    public static final Boolean ENABLED = Boolean.TRUE;
    public static final Boolean DISABLED = Boolean.FALSE;
    public static String ESTADO = "1";

}
