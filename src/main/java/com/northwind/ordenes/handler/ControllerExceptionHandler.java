package com.northwind.ordenes.handler;


import com.northwind.ordenes.dto.ExceptionResponseDTO;
import com.northwind.ordenes.exceptions.AccessDataException;
import com.northwind.ordenes.exceptions.ReadAccessDataExecption;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;



@ControllerAdvice
@RestController
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler{

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handlerAllException(Exception exception, WebRequest request){

        ExceptionResponseDTO exceptionResponseDTO = new ExceptionResponseDTO( LocalDateTime.now(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value(),
                HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), request.getDescription(false), exception.getStackTrace(), exception.getMessage());

        exception.printStackTrace();

        return new ResponseEntity<>(exceptionResponseDTO, exceptionResponseDTO.getHttpStatus());
    }


    @ExceptionHandler(AccessDataException.class)
    public ResponseEntity<Object> handlerAccessDataException(AccessDataException exception, WebRequest request){

        ExceptionResponseDTO exceptionResponseDTO = new ExceptionResponseDTO( LocalDateTime.now(), HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.value(),
                HttpStatus.BAD_REQUEST.getReasonPhrase(), request.getDescription(false),  exception.getStackTrace(),exception.getMessage());

        exception.printStackTrace();

        return new ResponseEntity<>(exceptionResponseDTO, exceptionResponseDTO.getHttpStatus());
    }


    @ExceptionHandler(ReadAccessDataExecption.class)
    public ResponseEntity<Object> handlerReadAccessDataException(ReadAccessDataExecption exception, WebRequest request){

        ExceptionResponseDTO exceptionResponseDTO = new ExceptionResponseDTO( LocalDateTime.now(), HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND.value(),
                HttpStatus.NOT_FOUND.getReasonPhrase(), request.getDescription(false), exception.getStackTrace(), exception.getMessage());

        exception.printStackTrace();

        return new ResponseEntity<>(exceptionResponseDTO, exceptionResponseDTO.getHttpStatus());
    }


}