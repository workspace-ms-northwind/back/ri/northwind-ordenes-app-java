package com.northwind.ordenes.client;


import com.northwind.ordenes.dto.feign.CustomerDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient( name = "customerClient", url = "http://localhost:8083/Clients")
public interface CustomerClientFeign {

    @GetMapping(value = "/client/{id}", produces = "application/json")
    CustomerDTO findById(@PathVariable(name = "id") String id);

}
