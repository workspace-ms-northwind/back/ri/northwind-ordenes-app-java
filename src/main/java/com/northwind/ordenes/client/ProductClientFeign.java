package com.northwind.ordenes.client;


import com.northwind.ordenes.dto.feign.ProductDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient( name = "productClient", url = "http://localhost:8081/Products")
public interface ProductClientFeign {

    @GetMapping(value = "/product/{id}", produces = "application/json")
    ProductDTO findById(@PathVariable(name = "id") Long id);


}
