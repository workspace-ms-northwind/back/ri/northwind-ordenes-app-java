package com.northwind.ordenes.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

@Builder
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tb_nw_order_details")
@IdClass(TpOrderDetailId.class)
public class TpOrderDetail {

    @Id
    @NotNull
    @Column(name = "order_id", nullable = false)
    private Long orderId;

    @Id
    @NotNull
    @Column(name = "product_id", nullable = false, insertable = false, updatable = false)
    private Long productId;

    @NotNull
    @Column(name = "unit_price", nullable = false)
    private BigDecimal unitPrice;

    @NotNull
    @Column(name = "quantity", nullable = false)
    private Short quantity;

    @NotNull
    @Column(name = "discount", nullable = false)
    private BigDecimal discount;

    @Column(name = "enabled")
    private Boolean enabled;

    @MapsId("orderId")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(
            name = "order_id"
            , referencedColumnName = "order_id"
            , nullable = false
            , insertable = false
            , updatable = false)
    @ToString.Exclude
    @JsonBackReference
    private TpOrder order;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TpOrderDetail that = (TpOrderDetail) o;
        return getOrderId() != null && Objects.equals(getOrderId(), that.getOrderId())
                && getProductId() != null && Objects.equals(getProductId(), that.getProductId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, productId);
    }
}