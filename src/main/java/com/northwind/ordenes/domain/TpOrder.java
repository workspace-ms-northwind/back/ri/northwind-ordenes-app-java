package com.northwind.ordenes.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "tb_nw_orders")
public class TpOrder {

    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sc_generator_key_tp_mw_orders"
    )
    @SequenceGenerator(
            name = "sc_generator_key_tp_mw_orders",
            sequenceName = "sc_generator_key_tp_mw_orders_db",
            initialValue = 11080,
            allocationSize = 1
    )
    @Column(name = "order_id", nullable = false)
    private Long id;

    @Column(name = "order_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "required_date")
    private Date requiredDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "shipped_date")
    private Date shippedDate;

    @Column(name = "freight")
    private Float freight;

    @Size(max = 40)
    @Column(name = "ship_name", length = 40)
    private String shipName;

    @Size(max = 60)
    @Column(name = "ship_address", length = 60)
    private String shipAddress;

    @Size(max = 40)
    @Column(name = "ship_city", length = 40)
    private String shipCity;

    @Size(max = 40)
    @Column(name = "ship_region", length = 40)
    private String shipRegion;

    @Size(max = 10)
    @Column(name = "ship_postal_code", length = 10)
    private String shipPostalCode;

    @Size(max = 40)
    @Column(name = "ship_country", length = 40)
    private String shipCountry;


    @Column(name = "enabled")
    private Boolean enabled;

    @Column(name = "customer_id", length = 5)
    private String customer;

    @Column(name = "employee_id")
    private Long employee;

/*    @OneToMany( mappedBy = "order" ) //, cascade = CascadeType.ALL)
    @ToString.Exclude
    @JsonManagedReference
//  @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    private List<TpOrderDetail> orderDetails = new ArrayList<>();*/

    @ManyToOne
    @JoinColumn(name = "ship_via", referencedColumnName = "shipper_id")
    @ToString.Exclude
//  @JsonIgnore
    private TpShipper shipVia;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TpOrder tpOrder = (TpOrder) o;
        return getId() != null && Objects.equals(getId(), tpOrder.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}