package com.northwind.ordenes.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "tb_nw_shippers")
public class TpShipper {

    @Id
    @Column(name = "shipper_id", nullable = false)
    private Long id;

    @Size(max = 40)
    @NotNull
    @Column(name = "company_name", nullable = false, length = 40)
    private String companyName;

    @Size(max = 24)
    @Column(name = "phone", length = 24)
    private String phone;


    @Column(name = "enabled")
    private Boolean enabled;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TpShipper tpShipper = (TpShipper) o;
        return getId() != null && Objects.equals(getId(), tpShipper.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}