package com.northwind.ordenes.repository;

import com.northwind.ordenes.domain.TpOrder;
import com.northwind.ordenes.domain.TpOrderDetail;
import com.northwind.ordenes.domain.TpOrderDetailId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderDetailRepository extends JpaRepository<TpOrderDetail, TpOrderDetailId> {
    List<TpOrderDetail> findAllByEnabled(Boolean enable );

    Page<TpOrderDetail> findAllByEnabled(Pageable page, Boolean enable );
    List<TpOrderDetail> findAllByOrderIdAndEnabled(Long orderId, Boolean enable );
}
