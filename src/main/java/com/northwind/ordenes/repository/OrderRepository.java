package com.northwind.ordenes.repository;

import com.northwind.ordenes.domain.TpOrder;
import com.northwind.ordenes.dto.PageRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<TpOrder, Long> {
    List<TpOrder> findAllByEnabled(Boolean enable );

    Page<TpOrder> findAllByEnabled(Pageable page, Boolean enable );
    Optional<TpOrder> findByIdAndEnabled(Long id, Boolean enable );
}
