package com.northwind.ordenes.mappers;

import com.northwind.ordenes.domain.TpOrder;
import com.northwind.ordenes.dto.OrderDetailDTO;
import com.northwind.ordenes.domain.TpOrderDetail;
import com.northwind.ordenes.dto.feign.ProductDTO;
import org.hibernate.criterion.Order;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface OrderDetailMapper {

    @Mapping(target = "order",  ignore = true)
    @Mapping(source = "habilitado", target = "enabled")
    @Mapping(source = "descuento", target = "discount")
    @Mapping(source = "cantidad", target = "quantity")
    @Mapping(source = "precioUnitario", target = "unitPrice")
    @Mapping(source = "productoId", target = "productId")
    @Mapping(source = "pedidoId", target = "orderId")
    TpOrderDetail orderDetailDTOToTpOderDetail(OrderDetailDTO orderDetailDTO, @Context TpOrder ordered);

    List<TpOrderDetail> orderDetailDTOListToTpOderDetailList(List<OrderDetailDTO> orderDetailDTOList, @Context TpOrder tpOrder);

    @AfterMapping
    default void afterOrderDetailDTOToTpOderDetailList(@MappingTarget TpOrderDetail tpOrderDetail, @Context TpOrder tpOrder) {
        tpOrderDetail.setOrderId(tpOrder.getId());
    }

    List<OrderDetailDTO> tpOrderDetailListToOrderDetailDTOList(List<TpOrderDetail> tpOrderDetail);

    @InheritInverseConfiguration(name = "orderDetailDTOToTpOderDetail")
    @Mapping(target = "product", ignore = true)
    @Mapping(source = "unitPrice", target = "precioUnitario", numberFormat = "$#.00")
    OrderDetailDTO tpOrderDetailToOrderDetailDTO(TpOrderDetail tpOrderDetail);


 }