package com.northwind.ordenes.mappers;

import com.northwind.ordenes.dto.OrderRequestDTO;
import com.northwind.ordenes.domain.TpOrder;
import com.northwind.ordenes.dto.OrderResponseDTO;
import com.northwind.ordenes.dto.feign.CustomerDTO;
import org.mapstruct.*;

import java.util.List;

@Mapper( componentModel = "spring", uses = {OrderDetailMapper.class, ShipperMapper.class},  builder = @Builder(disableBuilder = true))
public interface OrderMapper {
    @Mapping(source = "empleado", target = "employee")
    @Mapping(source = "cliente", target = "customer")
    @Mapping(source = "habilitado", target = "enabled")
    @Mapping(source = "paisEnvio", target = "shipCountry")
    @Mapping(source = "codigoPostalEnvio", target = "shipPostalCode")
    @Mapping(source = "regionEnvio", target = "shipRegion")
    @Mapping(source = "ciudadEnvio", target = "shipCity")
    @Mapping(source = "direccionEnvio", target = "shipAddress")
    @Mapping(source = "nombreEnvio", target = "shipName")
    @Mapping(source = "flete", target = "freight")
    @Mapping(source = "fechaEnvio", target = "shippedDate")
    @Mapping(source = "fechaRequerida", target = "requiredDate")
    @Mapping(source = "fechaPedido", target = "orderDate")
    @Mapping(source = "pedidoId", target = "id")
    TpOrder orderDTOToTpOrder(OrderRequestDTO orderDTO);

/*  @AfterMapping
    default void enlazarPedidoDetalle(@MappingTarget TpOrder tpOrder) {
        tpOrder.getOrderDetails().forEach(orderDetail -> orderDetail.setOrder(tpOrder));
    }*/

//    @InheritInverseConfiguration(name = "orderDTOToTpOrder")
    @Mapping( target = "empleado", source = "tpOrder.employee")
    @Mapping( target = "habilitado", source = "tpOrder.enabled")
    @Mapping( target = "paisEnvio", source = "tpOrder.shipCountry")
    @Mapping( target = "codigoPostalEnvio", source = "tpOrder.shipPostalCode")
    @Mapping( target = "regionEnvio", source = "tpOrder.shipRegion")
    @Mapping( target = "ciudadEnvio", source = "tpOrder.shipCity")
    @Mapping( target = "direccionEnvio", source = "tpOrder.shipAddress")
    @Mapping( target = "nombreEnvio", source = "tpOrder.shipName")
    @Mapping( target = "flete", source = "tpOrder.freight")
    @Mapping( target = "fechaEnvio", source = "tpOrder.shippedDate")
    @Mapping( target = "fechaRequerida", source = "tpOrder.requiredDate")
    @Mapping( target = "fechaPedido", source = "tpOrder.orderDate")
    @Mapping( target = "pedidoId", source = "tpOrder.id")
    @Mapping(target = "cliente", source = "customer")
    OrderResponseDTO tpOrderToOrderDTO(TpOrder tpOrder, CustomerDTO customer);

//    List<OrderResponseDTO> tpOrderListToOrderDTOList(List<TpOrder> tpOrder, CustomerDTO customer);


}