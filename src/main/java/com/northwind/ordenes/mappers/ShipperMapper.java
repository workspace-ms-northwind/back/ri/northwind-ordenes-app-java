package com.northwind.ordenes.mappers;

import com.northwind.ordenes.dto.ShipperDTO;
import com.northwind.ordenes.domain.TpShipper;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ShipperMapper {
    @Mapping(source = "habilitado", target = "enabled")
    @Mapping(source = "telefono", target = "phone")
    @Mapping(source = "nombreCompania", target = "companyName")
    TpShipper shipperDTOToTpShipper(ShipperDTO shipperDTO);

    @InheritInverseConfiguration(name = "shipperDTOToTpShipper")
    ShipperDTO tpShipperToShipperDTO(TpShipper tpShipper);


}