package com.northwind.ordenes.exceptions;



public class AccessDataException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AccessDataException(String message) {
		super(message);
	}

}
