package com.northwind.ordenes.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ReadAccessDataExecption extends AccessDataException {

	private static final long serialVersionUID = 6491305126042680540L;

	public ReadAccessDataExecption(String message) {
		super(message);
	}

}
