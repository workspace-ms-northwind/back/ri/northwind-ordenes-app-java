package com.northwind.ordenes.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Objects;

@Getter
@Setter
public class PageRequestDTO {

    private Integer page = 0;
    private Integer sizePage = 10;
    private Sort.Direction direction = Sort.Direction.ASC;
    private String sortByColumn = "id";

    public Pageable getPageable(PageRequestDTO dto){

        Integer page = Objects.nonNull(dto.getPage()) ? dto.getPage() : this.page;
        Integer size = Objects.nonNull(dto.getSizePage()) ? dto.getSizePage() : this.sizePage;
        Sort.Direction sort = Objects.nonNull(dto.getDirection()) ? dto.getDirection() : this.direction;
        String sortByColumn = Objects.nonNull(dto.getSortByColumn()) ? dto.getSortByColumn() : this.sortByColumn;

        return PageRequest.of(page,size, sort, sortByColumn);

    }

}
