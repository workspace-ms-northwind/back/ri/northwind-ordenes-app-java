package com.northwind.ordenes.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShipperDTO implements Serializable {

    Long id;

    @NotNull
    @Size(max = 40)
    String nombreCompania;

    @Size(max = 24)
    String telefono;

    Boolean habilitado;

}