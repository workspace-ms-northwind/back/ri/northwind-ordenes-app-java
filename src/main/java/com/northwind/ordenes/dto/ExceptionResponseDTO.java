package com.northwind.ordenes.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class ExceptionResponseDTO {

    @JsonProperty("timeStamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime timestamp;

    @JsonIgnore
    private HttpStatus httpStatus;

    @JsonProperty("status")
    private int value;

    @JsonProperty("error")
    private String reasonPhrase;

    private String details;

    @JsonIgnore
    private StackTraceElement[] trace;

    private String message;

}
