package com.northwind.ordenes.dto;

import com.northwind.ordenes.dto.feign.CustomerDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderResponseDTO implements Serializable {

    Long pedidoId;

    Date fechaPedido;

    Date fechaRequerida;

    Date fechaEnvio;

    Float flete;

    @Size(max = 40)
    String nombreEnvio;

    @Size(max = 60)
    String direccionEnvio;

    @Size(max = 40)
    String ciudadEnvio;

    @Size(max = 40)
    String regionEnvio;

    @Size(max = 10)
    String codigoPostalEnvio;

    @Size(max = 40)
    String paisEnvio;

    Boolean habilitado;

    CustomerDTO cliente;

    Long empleado;

    List<OrderDetailDTO> orderDetails;

    ShipperDTO shipVia;

}