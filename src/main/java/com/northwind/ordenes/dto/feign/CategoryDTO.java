package com.northwind.ordenes.dto.feign;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDTO implements Serializable {

    @JsonProperty("idCategoria")
    Long idCategoria;

    @JsonProperty("nombre")
    @NotNull
    @Size(max = 15)
    String nombre;

    @JsonProperty("descripcion")
    String descripcion;

    @JsonProperty("estado")
    String estado;

    @JsonProperty("imagen")
    byte[] imagen;
}