package com.northwind.ordenes.dto.feign;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO implements Serializable {
    String id;
    @NotNull
    @Size(max = 40)
    String nombreCompania;
    @Size(max = 100)
    String nombreContacto;
    @Size(max = 100)
    String tituloContacto;
    @Size(max = 60)
    String direccion;
    @Size(max = 30)
    String ciudad;
    @Size(max = 30)
    String region;
    @Size(max = 30)
    String codigoPostal;
    @Size(max = 30)
    String pais;
    @Size(max = 24)
    String telefono;
    @Size(max = 24)
    String fax;
}