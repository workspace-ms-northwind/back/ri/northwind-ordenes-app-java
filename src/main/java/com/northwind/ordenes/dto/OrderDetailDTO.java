package com.northwind.ordenes.dto;

import com.northwind.ordenes.dto.feign.ProductDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetailDTO implements Serializable {

    @NotNull
    Long pedidoId;

    @NotNull
    Long productoId;

    @NotNull
    String precioUnitario;

    @NotNull
    Short cantidad;

    @NotNull
    BigDecimal descuento;

    Boolean habilitado;

    ProductDTO product;

}