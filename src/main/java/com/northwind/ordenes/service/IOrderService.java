package com.northwind.ordenes.service;

import com.northwind.ordenes.dto.OrderRequestDTO;
import com.northwind.ordenes.dto.OrderResponseDTO;
import com.northwind.ordenes.dto.PageRequestDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IOrderService {
    List<OrderRequestDTO> findAll();
    Page<OrderResponseDTO> findAll(PageRequestDTO pageable);
    OrderResponseDTO save(OrderRequestDTO entity);
    OrderResponseDTO findById(Long id);
    OrderResponseDTO changeStatus(OrderRequestDTO entity);

}
