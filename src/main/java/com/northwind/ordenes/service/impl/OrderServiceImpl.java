package com.northwind.ordenes.service.impl;

import com.northwind.ordenes.client.CustomerClientFeign;
import com.northwind.ordenes.client.ProductClientFeign;
import com.northwind.ordenes.domain.TpOrder;
import com.northwind.ordenes.domain.TpOrderDetail;
import com.northwind.ordenes.dto.OrderRequestDTO;
import com.northwind.ordenes.dto.OrderDetailDTO;
import com.northwind.ordenes.dto.OrderResponseDTO;
import com.northwind.ordenes.dto.PageRequestDTO;
import com.northwind.ordenes.dto.feign.CustomerDTO;
import com.northwind.ordenes.dto.feign.ProductDTO;
import com.northwind.ordenes.exceptions.ReadAccessDataExecption;
import com.northwind.ordenes.mappers.OrderDetailMapper;
import com.northwind.ordenes.mappers.OrderMapper;
import com.northwind.ordenes.repository.OrderDetailRepository;
import com.northwind.ordenes.repository.OrderRepository;
import com.northwind.ordenes.service.IOrderService;
import com.northwind.ordenes.util.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements IOrderService {

    private final OrderRepository repository;
    private final OrderDetailRepository detailRepository;
    private final OrderMapper mapper;
    private final OrderDetailMapper detailMapper;
    private final ProductClientFeign productClient;
    private final CustomerClientFeign customerFeign;

    @Override
    public List<OrderRequestDTO> findAll() {

/*        var domainProdcutList = repository.findAllByEnabled(Constants.ENABLED);

        return  mapper.tpOrderListToOrderDTOList(domainProdcutList);*/
        return null;

    }

    @Override
    public Page<OrderResponseDTO> findAll(PageRequestDTO pageRequestDTO) {

        Pageable pageable = new PageRequestDTO().getPageable(pageRequestDTO);
        Page<TpOrder> domainProdcutList = repository.findAllByEnabled(pageable, Constants.ENABLED);

        Pageable pageable1 =  domainProdcutList.getPageable();
        long cantidad = domainProdcutList.getTotalElements();

        List<TpOrder> content = domainProdcutList.getContent();



//        List<OrderResponseDTO>  orderDTOList = new ArrayList<>();
       /* for ( TpOrder order: content) {
            CustomerDTO customer = customerFeign.findById(order.getCustomer());
            orderDTOList.add(mapper.tpOrderToOrderDTO(order, customer));
        }*/
        List<OrderResponseDTO>  orderDTOList = content.stream().map( order -> {
                    CustomerDTO customer = customerFeign.findById(order.getCustomer());
                    OrderResponseDTO responseDTO =  mapper.tpOrderToOrderDTO(order, customer);
                    return responseDTO;
                }
        ).collect(Collectors.toList());

//        List<OrderResponseDTO>  orderDTOList = mapper.tpOrderListToOrderDTOList(content, );

        return new PageImpl<>(orderDTOList, pageable1, cantidad );


    }

    @Override
    public OrderResponseDTO save(OrderRequestDTO entity) {

        TpOrder order = mapper.orderDTOToTpOrder(entity);
        TpOrder domainOrder = repository.save(order);

        List<OrderDetailDTO> detailDTOList = entity.getOrderDetails();
        List<TpOrderDetail> tpOrderDetails =  detailMapper.orderDetailDTOListToTpOderDetailList(detailDTOList, domainOrder);

        List<TpOrderDetail> domaingOrderDeaitls = detailRepository.saveAll(tpOrderDetails);

        List<OrderDetailDTO> responseDetailDTOS = detailMapper.tpOrderDetailListToOrderDetailDTOList(domaingOrderDeaitls);

        CustomerDTO customer = customerFeign.findById(domainOrder.getCustomer());

        var response = mapper.tpOrderToOrderDTO(domainOrder, customer);
        response.setOrderDetails(responseDetailDTOS);
        return response;
    }


    @Override
    public OrderResponseDTO findById(Long id) {

        Optional<TpOrder> domain = repository.findByIdAndEnabled(id, Constants.ENABLED);
        if (domain.isEmpty()) {
            throw new ReadAccessDataExecption("No se encontró el Pedido");
        }
        TpOrder domainOrder = domain.get();

        List<TpOrderDetail> domainOrderDetails = detailRepository.findAllByOrderIdAndEnabled(domainOrder.getId(), Constants.ENABLED);

        List<OrderDetailDTO> orderDetailDTOList = detailMapper.tpOrderDetailListToOrderDetailDTOList(domainOrderDetails);

        List<OrderDetailDTO> orderDetailDTOList2 = orderDetailDTOList.stream().map( orderDetailDTO -> {
                   ProductDTO productDTO = productClient.findById(orderDetailDTO.getProductoId());
                   orderDetailDTO.setProduct(productDTO);
                   return orderDetailDTO;
                }
        ).collect(Collectors.toList());

        CustomerDTO customer = customerFeign.findById(domainOrder.getCustomer());

        OrderResponseDTO orderDTO = mapper.tpOrderToOrderDTO(domainOrder, customer);
        orderDTO.setOrderDetails(orderDetailDTOList2);
        return orderDTO;
    }



    @Override
    public OrderResponseDTO changeStatus(OrderRequestDTO entity) {

        Optional<TpOrder> domain = repository.findById(entity.getPedidoId());
        if (domain.isEmpty()) {
            throw new ReadAccessDataExecption("No se encontro el Producto");
        }

        TpOrder domainOrder = domain.get();

        if (domainOrder.getId().equals(entity.getPedidoId())) {
            domainOrder.setEnabled(false);
            CustomerDTO customer = customerFeign.findById(domainOrder.getCustomer());
            return mapper.tpOrderToOrderDTO(repository.save(domainOrder), customer);
        } else {
            throw new ReadAccessDataExecption("El nombre del Producto no coincide");
        }
    }

}
